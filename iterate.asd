;;; -*- lisp -*- system definition

(defsystem :iterate
  :description "Jonathan Amsterdam's iterator/gatherer/accumulator facility"
  :licence "MIT"
  :in-order-to ((test-op (test-op :iterate/tests)))
  :components ((:file "package")
               (:file "iterate" :depends-on ("package"))))

(defsystem :iterate/tests
  :depends-on (:iterate #+sbcl :sb-rt #-sbcl :rt)
  :components ((:file "iterate-test"))
  :perform (test-op (o c)
		    (uiop:symbol-call #+sbcl :sb-rt
				      #-sbcl :regression-test
				      '#:do-tests)))
